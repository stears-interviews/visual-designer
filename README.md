# Stears Visual Design Test

Thanks for your application to Stears

This interview is intended for a designer with knowledge / experience in Product design and Graphics design

We ask that you complete and submit a single open ended task for assessment

#### Submissions

- Send an email to victor_enesi@stearsng.com and foluso_ogunlana@stearsng.com with your submission
- Leave detailed descriptions and explanations in the email if necessary

#### Deadlines

You will be given at least 5 working days to complete the task and your deadline will be communicated via email.

#### Assessment

Our favourite submissions are -

1. **Rigorous** - Minimal solutions that get to the heart of the problem to solve and stop there
2. **Creative and Originality** - We want to see your approach to solving a problem. You approach does not have to be out of the box, but it must be original
3. **UX & DevX** - Smooth user experience which is easy to use and simple to implement for engineers
4. **Clean** - Neat and well documented research findings, methodology, designs, wireframes, assets, etc.
   ...

Submissions that impress us will demonstrate the following -

- **Good problem solving approach** - to solving the problem - leading to a consistent, reliable and repeatable process
- **Mastery** - in your technical product and graphics design skills and use of tools and patterns

#### Conduct

By submitting, you assure us that you have not assigned the test to anyone else and that all work submitted is your own. Though you are allowed to use whatever online or offline resources you see fit to learn skills for the tasks. You are also permitted to interact with your peers for research purposes.

# **Test**

**Guidelines - READ THE FOLLOWING INSTRUCTIONS CAREFULLY**

- Finish task 1 only
- Zip your files and share via email
- Implement your solutions using a combination of tools like Adobe Photoshop, Illustrator, Adobe XD or Figma

## Task 1

The Stears Business product team is currently looking to build the next major feature on the platform in the coming months. They have a myriad of ideas on which feature to build based on suggestions from their users, customers and investors, but the team can’t seem to figure out which of the feature(s) should be prioritized for the best ROI.

As a member of the Stears Business team, your task should you choose to accept it is to execute the following deliverables:

- List out the possible ideas that the team could implement and help determine which of them should be prioritized. (You'll need to back this up with logical reasoning or data)
- Create a prototype of three screens that best implements this feature using Adobe XD or Figma
- Create a marketing campaign for this feature. The campaign should be made for two social platforms you think are consistently used by Stears Business.

## Thanks!

If you made it this far, thanks for your time.
We look forward to reviewing your solid application with you!
